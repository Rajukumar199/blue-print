$subscription = '060c7910-de04-45a3-9953-ad495ae2be2b'
Install-Module -Name Az.Blueprint -Repository PSGallery -MinimumVersion 0.2.11 -AllowClobber -Force -Verbose
Import-Module -Name Az.Blueprint
write-output "starting of pipeline"
Connect-AzAccount -Identity


cd .\Artifacts
$blueprint = New-AzBlueprint -Name 'MyBlueprint-PCI4' -BlueprintFile 'blueprint.json'

New-AzBlueprintArtifact -Blueprint $blueprint -Name 'Allowed_locations_for_resource_groups' -ArtifactFile 'Allowed_locations_for_resource_groups.json'
New-AzBlueprintArtifact -Blueprint $blueprint -Name 'Allowed_locations' -ArtifactFile 'Allowed_locations.json'
New-AzBlueprintArtifact -Blueprint $blueprint -Name 'controls_and_deploy' -ArtifactFile 'controls_and_deploy.json'
New-AzBlueprintArtifact -Blueprint $blueprint -Name 'Deploy_Auditing_on_SQL_servers' -ArtifactFile 'Deploy_Auditing_on_SQL_servers.json'
New-AzBlueprintArtifact -Blueprint $blueprint -Name 'Deploy_SQL_DB_transparent_data_encryption' -ArtifactFile 'Deploy_SQL_DB_transparent_data_encryption.json'
New-AzBlueprintArtifact -Blueprint $blueprint -Name 'Deploy_Threat_Detection_on_SQL_servers' -ArtifactFile 'Deploy_Threat_Detection_on_SQL_servers.json'
New-AzBlueprintArtifact -Blueprint $blueprint -Name 'Require_encryption_on_Data_Lake_Store_accounts' -ArtifactFile 'Require_encryption_on_Data_Lake_Store_accounts.json'

Publish-AzBlueprint -Blueprint $blueprint -Version 1.5

New-AzBlueprintAssignment -Blueprint $blueprint -Name 'assignMyBlueprint123' -Location "East US" -SubscriptionId $subscription -Parameter @{deployAuditingonSQLservers_storageAccountsResourceGroup='delete'}


  